#ifndef TERMENGINEFACTORY_H
#define TERMENGINEFACTORY_H
#include "termEngine.hpp"

#ifdef _WIN32
    #error No support for Windows OS
#elif __linux__
    #include "termEngineLinux.hpp"
#else
    #error Unsupportet OS
#endif

class TermEngineFactory {
public:
    static TermEngine* getEngine() {
#ifdef _WIN32
        return nullptr;
#elif __linux__
        return new TermEngineLinux();
#else
        return nullptr;
#endif
    }
};

#endif /* !TERMENGINEFACTORY_H */

