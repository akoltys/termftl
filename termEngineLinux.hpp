#ifndef TERMENGINELINUX_H
#define TERMENGINELINUX_H

#include "fpsCounter.hpp"
#include "termEngine.hpp"

class TermEngineLinux: public TermEngine {
public:
    TermEngineLinux();
    ~TermEngineLinux();
    std::vector<int> getInput();
    int getWidth();
    int getHeight();
    void draw();
    void draw(int posX, int posY, std::vector<std::string> &data);
private:
    void update();

    FpsCounter mFps;
};

#endif /* !TERMENGINELINUX_H */

