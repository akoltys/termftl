#ifndef TERMFPP_H
#define TERMFPP_H

#include <math.h>
#include "termEngineFactory.hpp"

class TermFpp {
public:
    TermFpp();
    ~TermFpp();
    void run();
protected:
    TermEngine *mEngine = nullptr;
    std::vector<std::string> mMap;
    float mFov = M_PI/2; // Field of view
    float mAngle = 0.0f; // the angle at which the player is currently looking
    int mPlayerPosX = 0; // player position X coordinate
    int mPlayerPosY = 0; // player position Y coordinate
    int mLos = 25; // Line of sight how far player can see

    int getMapWidth() { return (mMap.size() > 0) ? mMap[0].size() : 0; };
    int getMapHeight() { return mMap.size(); }
};

#endif /* !TERMFPP */

