#ifndef TERMENGINE_H
#define TERMENGINE_H

#include <string>
#include <vector>

#include <curses.h>

class TermEngine {
public:
    enum COLOR {
        INVISIBLE = 0,
        EMPTY,
        WHITE,
        GROUND,
        WATER,
        SKY,
        GRASS,
        MAX
    };

    virtual std::vector<int> getInput() = 0;
    virtual int getWidth() = 0;
    virtual int getHeight() = 0;
    virtual void draw() = 0;
    std::vector<COLOR>& getScreenBuffer() { return mBuffer; }
protected:
    std::vector<COLOR> mBuffer;
};

#endif /* !TERMENGINE_H */

