#ifndef FPSCOUNTER_H
#define FPSCOUNTER_H

#include <chrono>
#include <numeric>
#include <vector>

class FpsCounter {
    public:
        FpsCounter() {
            mCurr = std::chrono::high_resolution_clock::now();
            mPrev = std::chrono::high_resolution_clock::now();
        };
        virtual ~FpsCounter() {};
        double update() {
            mCurr =  std::chrono::high_resolution_clock::now();
            mFPS.push_back(1000/std::chrono::duration<double, std::milli>(mCurr-mPrev).count());
            mPrev = mCurr;
            if (mFPS.size() > 100) {
                mFPS.erase(mFPS.begin());
            }
            return std::accumulate(mFPS.begin(), mFPS.end(), 0.0f)/mFPS.size();
        }
        double getTimeDiff() {
            return std::chrono::duration<double, std::milli>(std::chrono::high_resolution_clock::now()-mPrev).count()/100*6;
        }
    private:
        std::vector<double> mFPS;
        std::chrono::high_resolution_clock::time_point mCurr;
        std::chrono::high_resolution_clock::time_point mPrev;
};

#endif /* !FPSCOUNTER_H */

