#include <sstream>
#include <string>
#include <vector>

#include <ncurses.h>
#include <unistd.h>

#include "termFpp.hpp"

constexpr unsigned int OFFSET_Y_MIN = 1;

int main(int argc, char *argv[]) 
{
    TermFpp game;
    game.run();
}
