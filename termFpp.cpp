#include "termFpp.hpp"


TermFpp::TermFpp()
{
    mEngine = TermEngineFactory::getEngine();
}

TermFpp::~TermFpp()
{
    delete mEngine;
}

void TermFpp::run()
{
    TermEngine *engine = TermEngineFactory::getEngine();
    FpsCounter fps;
    int offset_y = 2;
    int offset_x = 0;
//    int delta_y = 1;
//    int delta_x = 1;
    int widthMax = engine->getWidth();
    int heightMax = engine->getHeight();
    bool running = true;
    std::string tmpStr;
    std::vector<std::string> sprite;
    sprite.push_back("  ");

    mMap.push_back("####################");
    mMap.push_back("###            ### #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("#                  #");
    mMap.push_back("####################");
    mPlayerPosX = 2;
    mPlayerPosY = 2;
    while (running) {
        double timeDiff = fps.getTimeDiff();
        if (timeDiff < 1.0f) {
            continue;
        }
        fps.update();
        widthMax = engine->getWidth();
        heightMax = engine->getHeight();
        auto &screenBuffer = engine->getScreenBuffer();
        // handle input
        auto keys_pressed = engine->getInput();
        float deltaX = 0.0f;
        float deltaY = 0.0f;
        for(auto key: keys_pressed) {
            if (key == 'Q' || key == 'q') {
                running = false;
            } else if (key == 'W' || key == 'w') {
                deltaX += sin(mAngle)*2.0f;
                deltaY += cos(mAngle)*2.0f;
            } else if (key == 'S' || key == 's') {
                deltaX -= sin(mAngle)*2.0f;
                deltaY -= cos(mAngle)*2.0f;
            } else if (key == 'A' || key == 'a') {
                mAngle -= 0.1f; 
            } else if (key == 'D' || key == 'd') {
                mAngle += 0.1f;
            }
        }

        if (mMap[mPlayerPosY+deltaY][mPlayerPosX+deltaX] != '#') {
            mPlayerPosX += deltaX;
            mPlayerPosY += deltaY;
        }

        for(int x = 0; x < widthMax; x++) {
            float curAngle = mAngle - mFov/2 + mFov*((float)x/(float)widthMax);
            float curRayLength = 0.1f;
            bool wallFound = false;
            while(curRayLength <= mLos) {
                int tmpX = mPlayerPosX + (int)(curRayLength*sin(curAngle));
                int tmpY = mPlayerPosY + (int)(curRayLength*cos(curAngle));
                if (tmpX < 0 || tmpX >= getMapWidth() || tmpY < 0 || tmpY > getMapHeight() ) {
                    break;
                } else if (mMap[tmpY][tmpX] == '#') {
                    wallFound = true;
                    break;
                }
                curRayLength += 0.05f;
            }

            int top = heightMax/2;
            int bottom = heightMax-top;
            if (wallFound) {
                top = heightMax/2 - (heightMax/curRayLength);
                bottom = heightMax - top;
            }

            for (int y = 0; y < heightMax; y++) {
                if (y <= top) {
                    screenBuffer[widthMax*y+x] = TermEngine::COLOR::SKY;         
                } else if (y > top && y <= bottom) {
                    screenBuffer[widthMax*y+x] = TermEngine::COLOR::GRASS;         
                } else {
                    screenBuffer[widthMax*y+x] = TermEngine::COLOR::GROUND;         
                }
            }

            for (int y = 0; y < getMapHeight(); y++) {
                for (int x = 0; x < getMapWidth(); x++) {
                    if (x == mPlayerPosX && y == mPlayerPosY) {
                        screenBuffer[widthMax*(y+5)+x-getMapWidth()-5] = TermEngine::COLOR::WATER;
                    } else if (mMap[y][x] != '#') {
                        screenBuffer[widthMax*(y+5)+x-getMapWidth()-5] = TermEngine::COLOR::EMPTY;
                    } else {
                        screenBuffer[widthMax*(y+5)+x-getMapWidth()-5] = TermEngine::COLOR::WHITE;
                    }
                }
            }


        }

        engine->draw();
    }
}

