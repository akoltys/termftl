OUT_DIR = build

CC=g++
CPPFLAGS=-std=c++1z
LDFLAGS=-lncurses

EXECUTABLE=termFTL

_OBJ = 	main.o \
		termEngineLinux.o \
		termFpp.o

OBJ = $(patsubst %,$(OUT_DIR)/%, $(_OBJ))

all: termFTL

$(EXECUTABLE): directories $(OBJ)
	$(CC) -o ${OUT_DIR}/$@ $(OBJ) $(CPPFLAGS) $(LDFLAGS)

$(OUT_DIR)/%.o: %.cpp
	$(CC) -c -o $@ $< $(CPPFLAGS)

clean:
	rm -R -f ${OUT_DIR}

directories:
	mkdir -p ${OUT_DIR}
