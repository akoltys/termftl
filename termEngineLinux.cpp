#include <curses.h>
#include <sstream>

#include "termEngineLinux.hpp"

TermEngineLinux::TermEngineLinux()
{
    setlocale(LC_ALL, "");
    initscr();
    noecho();
    cbreak();
    nodelay(stdscr, TRUE);
    keypad(stdscr, TRUE);
    curs_set(FALSE);
    start_color();
    init_pair(EMPTY, COLOR_BLACK, COLOR_BLACK); 
    init_pair(GROUND, COLOR_BLACK, COLOR_YELLOW); 
    init_pair(WATER, COLOR_BLACK, COLOR_BLUE); 
    init_pair(SKY, COLOR_BLACK, COLOR_CYAN); 
    init_pair(GRASS, COLOR_BLACK, COLOR_GREEN); 
    init_pair(WHITE, COLOR_WHITE, COLOR_WHITE);
    mBuffer.resize(getWidth()*getHeight(), TermEngine::INVISIBLE);
}

TermEngineLinux::~TermEngineLinux()
{
}

int TermEngineLinux::getWidth()
{
    return COLS;
}

int TermEngineLinux::getHeight()
{
    return LINES;
}

std::vector<int> TermEngineLinux::getInput()
{
    std::vector<int> result;
    int tmp = getch();
    while(tmp != ERR) {
        result.push_back(tmp);
        tmp = getch();
    }
    return result;
}

void TermEngineLinux::draw()
{
    //clear();
    auto prevPixel = TermEngine::INVISIBLE;
    for (int idx = 0; idx < mBuffer.size(); idx++) {
        auto currPixel = mBuffer[idx];
        if (currPixel != TermEngine::INVISIBLE) {
            if (prevPixel != currPixel) {
                attroff(COLOR_PAIR(prevPixel));
                attron(COLOR_PAIR(currPixel));
            }
            mvaddch(idx/COLS, idx%COLS, ' ');
        }
        prevPixel = currPixel;
    }
    attroff(COLOR_PAIR(prevPixel));
    update();
}

void TermEngineLinux::update()
{
    std::stringstream statusBar;
    statusBar << "Width: " << COLS << " Height: " << LINES;
    statusBar << " FPS: " << mFps.update();
    mvprintw(0, 0, statusBar.str().data());
    if (getWidth()*getHeight() != mBuffer.size()) {
        mBuffer.resize(getWidth()*getHeight(), TermEngine::INVISIBLE);
    }
    std::fill(mBuffer.begin(), mBuffer.end(), TermEngine::COLOR::EMPTY);
    refresh();
}

